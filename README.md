# Servidor do Ovnitrap

## Descrição

Essa é uma aplicação [Django]() com [Django Rest Framework] que atua como a _API_ do projeto Ovnitrap.

## Execução

Para a correta execução do projeto, existem alguns pré-requisitos, a saber:

1. Instalar o [Docker](https://docs.docker.com/get-docker/), visto que o projeto está em um container, acompanhado dos arquivos _Dockerfile_ e _docker-compose.yml_.

1. Configurar as variáveis de ambiente:

   1. Na raiz do projeto, criar uma cópia do arquivo _.env.dist_ e nomear como _.env_.
   1. Substituir os valores das varíaveis de ambiente pelos seus valores reais.

1. Com o Docker ativo, execute o seguinte comando na raiz do projeto:

   ```
   docker compose up --build
   ```

1. Se nenhum erro se apresentar no terminal, o servidor estará executando no endereço:

   > [localhost:8000/api](http:localhost:8000/api)

## Resolução de Erros

Caso esteja ocorrendo algum erro relacionado à migrações do banco de dados, acesse o container em execução ou localmente e execute-as com os seguintes comandos na pasta **"ovnitrapserver/"**:

    docker compose exec server python ovnitrapserver/manage.py makemigrations

    docker compose exec server python ovnitrapserver/manage.py migrate

OU localmente:

    python manage.py makemigrations & python manage.py migrate

## Contribuição

- Se lembre de nunca commitar na **main**
- Evite commitar na **develop**
- Ao começar uma nova tarefa, **crie uma nova branch** a partir da **develop** identificando o que será feito. Ao concluir, crie um **Merge Request**.
