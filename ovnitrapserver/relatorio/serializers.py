from rest_framework import serializers
from .models import Relatorio

class RelatorioSerializer(serializers.ModelSerializer):
    atividade_larval = serializers.SerializerMethodField()

    class Meta:
        model = Relatorio
        fields = ['id', 'nivel_bateria', 'nivel_agua', 'temperatura', 'latitude', 'longitude', 'qtd_larvas', 'atividade_larval', 'data_hora', 'imagem', 'data_hora_descarga', 'armadilha']
        extra_kwargs = {
            'qtd_larvas': {'write_only': True}
        }

    def get_atividade_larval(self, obj):
        if obj.qtd_larvas == 0:
            return 0
        if obj.qtd_larvas <= 2:
            return 1
        elif obj.qtd_larvas <= 4:
            return 2
        elif obj.qtd_larvas <= 6:
            return 3
        elif obj.qtd_larvas <= 8:
            return 4
        else:
            return 5
