from django.shortcuts import get_object_or_404
from rest_framework.decorators import action
from django.http import FileResponse
from django.db.models import OuterRef, Subquery
from rest_framework import viewsets
from rest_framework.response import Response

from .models import Relatorio
from .serializers import RelatorioSerializer

class RelatorioViewSet(viewsets.ModelViewSet):
    queryset = Relatorio.objects.all()
    serializer_class = RelatorioSerializer

    @action(detail=True, methods=["get"])
    def imagem(self, _, pk=None):
        relatorio = get_object_or_404(Relatorio, pk=pk)
        imagem_path = relatorio.imagem.path
        return FileResponse(open(imagem_path, "rb"), content_type="image/jpeg")

    @action(detail=False, methods=["get"], url_path='recentes')
    def get_relatorios_recentes(self, request):
        subquery = Relatorio.objects.filter(armadilha=OuterRef('armadilha')).order_by('-data_hora').values('id')[:1]
        relatorios = Relatorio.objects.filter(id__in=Subquery(subquery))
        serializer = RelatorioSerializer(relatorios, many=True)
        return Response(serializer.data)