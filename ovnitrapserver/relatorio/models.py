from django.db import models
from armadilha.models import Armadilha


class Relatorio(models.Model):
    armadilha = models.ForeignKey(Armadilha, on_delete=models.CASCADE)
    nivel_bateria = models.DecimalField(max_digits=5, decimal_places=2)
    nivel_agua = models.DecimalField(max_digits=5, decimal_places=2)
    temperatura = models.DecimalField(max_digits=5, decimal_places=2)
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    qtd_larvas = models.IntegerField()
    data_hora = models.DateTimeField()
    imagem = models.ImageField(upload_to="images/")
    data_hora_descarga = models.DateTimeField()
