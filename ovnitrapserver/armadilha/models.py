import random
import string
from django.db import models


class Armadilha(models.Model):
    identificador = models.CharField(primary_key=True, max_length=5, unique=True, blank=True)
    apelido = models.TextField()

    def save(self, *args, **kwargs):
        if not self.identificador:
            self.identificador = self.gerar_identificador_unico()
        super().save(*args, **kwargs)

    def gerar_identificador_unico(self):
        caracteres = string.ascii_uppercase + string.digits
        identificador = "".join(random.choices(caracteres, k=5))
        return identificador

    def __str__(self):
        return self.identificador + " - " + self.apelido
