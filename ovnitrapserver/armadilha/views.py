from django.shortcuts import get_object_or_404
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action

from .models import Armadilha
from .serializers import ArmadilhaSerializer
from relatorio.models import Relatorio
from relatorio.serializers import RelatorioSerializer


class ArmadilhaViewSet(viewsets.ModelViewSet):
    queryset = Armadilha.objects.all()
    serializer_class = ArmadilhaSerializer

    @action(detail=True, methods=["get"])
    def relatorios(self, _, pk=None):
        armadilha = get_object_or_404(Armadilha, pk=pk)
        relatorios = Relatorio.objects.filter(armadilha=armadilha)
        serializer = RelatorioSerializer(relatorios, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
