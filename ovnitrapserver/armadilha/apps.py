from django.apps import AppConfig


class ArmadilhaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'armadilha'
