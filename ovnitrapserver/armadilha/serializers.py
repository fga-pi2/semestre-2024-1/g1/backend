from rest_framework import serializers

from .models import Armadilha
from relatorio.serializers import RelatorioSerializer


class ArmadilhaSerializer(serializers.ModelSerializer):
    relatorios = RelatorioSerializer(many=True, read_only=True)

    class Meta:
        model = Armadilha
        fields = "__all__"
