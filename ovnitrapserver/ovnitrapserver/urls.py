from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from rest_framework import routers

from armadilha.views import ArmadilhaViewSet
from relatorio.views import RelatorioViewSet

router = routers.DefaultRouter()
router.register(r"armadilhas", ArmadilhaViewSet)
router.register(r"relatorios", RelatorioViewSet)


urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include(router.urls)),
]
